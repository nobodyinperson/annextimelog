set positional-arguments

watch-test:
    watchexec -c -w . "poetry run -- sh -xc 'coverage run --source=annextimelog -m unittest -v -b; coverage html;coverage report'"

watch-mypy:
    watchexec -c -w . 'poetry run -- mypy .'

# test how a command-line event definition would be parsed (e.g. just test-tokens 10min ago until now work @home project=thatthing)
test-tokens *tokens:
    #!/bin/sh
    poetry run -- python -c '
    from annextimelog.token import Token
    from annextimelog.repo import Event
    from annextimelog.log import setup_logging
    import sys, shlex
    from rich import print
    from rich.pretty import Pretty
    from rich.table import Table
    from rich.align import Align
    from rich import box
    args = sys.argv[1:]
    setup_logging()
    table = Table(expand=True,title=f"{shlex.join(args)}\ninterpreted as ...", box=box.SIMPLE)
    table.add_column("Tokens",ratio=1, justify="left")
    table.add_column("Event",ratio=1, justify="center")
    tokens = list(Token.from_strings(args:=sys.argv[1:]))
    event = Event.from_tokens(tokens)
    table.add_row(Pretty(tokens), Align(event.to_rich(),"center",vertical="middle"))
    print(table)
    ' "$@"

# test against a given Python version
test-with-python-version *version:
    #!/bin/sh -x
    export LC_ALL=C.UTF-8
    python="$(poetry env info | tac | perl -ne 'print if s|^Executable:\s*(.+)$|$1|g' | head -n1)"
    nix-shell --pure -p poetry "python$(echo "$@" | perl -pe 's|\.||g')" --run "
        set -e
        poetry env use python"$1"
        poetry install
        poetry run python -m unittest -v -b
    "
    echo "Restoring old poetry env"
    poetry env use "$python"


# enter a shell with a temporary annextimelog repo with example data set up
temprepo:
    #!/bin/sh
    export ANNEXTIMELOGREPO=/tmp/annextimelog
    chmod -R +w "$ANNEXTIMELOGREPO"
    rm -rf "$ANNEXTIMELOGREPO"
    poetry run -- sh -c 'source doc/make-example-data.sh'
    poetry shell

# enter a shell with a temporary annextimelog repo with example data set up
asciinema *args:
    #!/bin/sh
    export ANNEXTIMELOGREPO=/tmp/annextimelog
    chmod -R +w "$ANNEXTIMELOGREPO"
    rm -rf "$ANNEXTIMELOGREPO"
    poetry run -- asciinema rec --overwrite doc/$(date +%Y%m%d%H%M%S)-annextimelog.cast -c 'clear;bash' "$@"

update-readme-help:
    #!/usr/bin/env python3
    import re, subprocess, shlex, sys
    with open(readme:="README.md",encoding="utf-8",errors="ignore") as fh:
      readmetext = fh.read()
    help_in_readme_regex = re.compile(r"^```bash\n(?P<help>usage: annextimelog.*?)^```$",flags=re.IGNORECASE | re.MULTILINE | re.DOTALL)
    if m:=help_in_readme_regex.search(readmetext):
      start, end = m.span("help")
      currenthelp = subprocess.check_output(shlex.split("poetry run atl --help"), encoding="utf-8", errors="ignore")
      readmetext = m.string[:start] + currenthelp + m.string[end:]
      with open(readme,"w",encoding="utf-8",errors="ignore") as fh:
        fh.write(readmetext)
    else:
      print(f"Couldn't find --help page in {readme!r} 🤷 Check the regex {help_in_readme_regex.pattern!r}.")
      sys.exit(1)
