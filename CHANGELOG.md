# `annextimelog` Changelog

## v0.14.0 - 30.07.2024 - Several improvements here and there and color=false config

- the comma separator for field values is only used if the value does not contain whitespace, e.g.:
    - `note="long sentence, with commas, will just be used verbatim"`
    - `note,="several words,with whitespace,still split by comma"`
    - `field=one,two,three,separate,values`
- auto-decline confirmation (with note) if not in an interactive session
- allow modifying metadata when continuning events, e.g. `atl cont work set project=A` or `atl cont coding 10min ago set tech=web`
- pretty tabular event format:
    - fix tabular event output for timepoints
    - color the border of events to indicate temporal openness and grey-out immutable field names
    - also show duration for currently running events
    - `atl ls` now sorts by end time, so recently finished events are listed last / closer to the next prompt, so are more visible
- fix missing `tzdata` module (problem on Windows)
- parse month names, e.g. `February`, `jan - mar`, `last dec - april`, `next april`, ...
- Allow `14:00 for 2h` (2h since 14:00) or `13 20min` (20min since 13:00), etc. as time ranges
- Allow 'YYYY-MM-DD YYYY-MM-DD' (two dates) as range
- Distinguish between event addition and movement in log output and git commit message
- Improve display of ”pretty” durations (`pretty_duration` is now a function and takes an argument to configure the displayed units)
- Fix handling of events with duplicate end times. Previously, the earlier end time was used, now the later one is used.
- Add `atl config color false` or `atl -c color=false ...` config option to disable colors (useful for dumb terminals)

## v0.13.1 - 17.01.2024 - Fix bug generating multiple start/end times

- fix a bug that could cause a warning that an event has multiple start/end times set (e.g. created by `atl tr work;atl stop work at 12;atl ls`)

## v0.13.0 - 17.01.2024 - Stopping and Continuing Events

- `atl stop`: allow specifying stop time, e.g. with `atl stop work today at 10` or just `atl stop work 10min ago` or `atl stop work 2h`
- `atl cont`: continue a past event as a new copied event starting at another time
- `atl mod`: when removing fields (e.g. `todo-=email`),  match with regex so not the whole value needs to be spelled out
- clean up tabular event format:
    - remove the first emoji column in pretty-formatted output (was a bit noisy)
    - put longer field values (with spaces in them) onto separate lines for readability
    - use 🔲 as `todo` bullet point

## v0.12.2 - 16.01.2024 -Hotfix for `atl stop`

- `atl stop` wrongly changed the ID to `open`

## v0.12.1 - 16.01.2024 -Hotfix for `atl stop`

- `atl stop` also modified events with an open start

## v0.12.0 - 16.01.2024 - Open intervals, points in time and Start/Stop Workflow

- Open intervals are now allowed!
- Events with identical start and end are handled as 'points in time' (don't show both start and end time)
- Closed events are now stored in folder structure `YYYY/MM` (previously, it was `YYYY/MM/DD` and `YYYY/WW`), open events in a new `open` folder.
- new time range wordings:
    ```
    in 10 weeks
    1 week ago
    monday 10:00 - tue 12
    ```
- show weeks in duration (not just days and smaller units)
- `atl edit`: when an ID condition (`id=asdf`) is given, it is not necessary anymore to have an actio separator like `set`, e.g. `atl ed id=asdf todo="follow up!"` is enough now.
- new property token:
    - `.open` matches events that have an open interval
    - `.openend` matches events without an end
    - `.openstart` matches events without a start
    - `.closed` matches events with both start and end defined
    - `.timepoint` matches points in time (when start and end are equal)
    - `.empty` matches if the event is empty
- 🩹 fix event matching when giving only a point in time or a property (e.g. `10h ago` or `.timepoint`)
- new `atl stop ...` command as alias for `atl edit set end=now if .open ...`
    - timewarrior-style usage is now possible:
        ```bash
        # in the morning
        atl tr work @office since 10
        # in the afternoon
        atl stop work
        ```

## v0.11.0 - 15.01.2024 - Recursive time range parsing and several improvements here and there

In this release, the time range parsing was rewritten to be recursive, enabling many more since/until combinations, such as:

```bash
10min ago unil now
2h since the last hour
work 1h @home
sleep @home since y20:00
meeting @office since 10min ago
this month until last tuesday
monday last week
tuesday next month
...
```

- fix `-O timeclock` output when an event has no tags or `account` field. Previously, hledger wouldn't recognize any tags on the transaction.
- remove `"repo"` key from `-O json` output
- `atl -c key` shortcut now means `atl -c key=true`
- annextimelog will now ask for confirmation before touching the data. Configurable via new `annextimelog.confirm=true|false` option (set with `atl config confirm true` or `atl -c confirm=false tr ...`)
- `atl ls` now sums up tracked time for the result
- Years (`2024`, `2023`, etc.) and months (`2024-01`, `01.2023`, `04/2024`, `2022/04`) are now supported
    - mostly useful with `atl ls 2023` for example
    - ⚠️  This means `1500` now doesn't parse as 'today 15:00' anymore but as year 1500!
- `atl ls todo` now matches both events that have e.g. a `todo` tag and a `todo` metadata field. This makes it easier to use `atl` as a todo list: It will list events like `atl tr meeting today todo='remember this!'` and `atl tr today phonecall with=marc todo`.
- more easily typable event IDs like `bulazimuvubofowe`, `gibexuhofudezexi`, etc. are now used.
- `atl ls` now lists events sorted by time

## 0.10.1 - 12.01.2024 - Fix warning

- fix the `Overwriting previous 'field' values` warning

## 0.10.0 - 12.01.2024 - Editing events

- new `atl edit` (or `mod`) command to edit events with the same syntax as `atl tr` and `atl ls` combined, e.g. `atl edit where id=WVQtc8nX set project=secret` or `atl mod @home if gardening`, ...
- use `·` instead of `📝` to separate/indicate custom metadata field values
- fix `atl ls -l`

## 0.9.0 - 10.01.2024 - Massive code refactoring, weekdays support and other improvements

- massive code base refactoring, introducing a class for an annextimelog repository, reducing code duplucation and streamlining handling events.
- The `location` field can now be set via `@home`, `at=home`, `where=home`, `loc=home`, `location=home`, `at=Berlin,Germany` (multiple values).
- `annextimelog.*` git config options are now auto-documented in `atl --help`
- new time range wordings:
    - `until today`, `since yesterday`, `until last week`, `since last month`, ...
    - `mo - fr`, `Tue to Mon` (until next monday),  `since last monday`, `until next Fri`, `since mo`, `until fr`, ...

## v0.8.0 - 09.01.2024 - Event search

- `atl ls`: match with same language as `atl tr`, e.g. `atl ls @home`, `atl ls yesterday`, `atl ls work last week on project='my.*regex'`
- `atl tr`: allow `this week`, `next month`, `last hour`, etc...
- `atl del`: fix deleting multiple events
- new `atl config` convenience wrapper around `atl git config`, e.g. `atl config emojis false` instead of `atl git config annextimelog.emojis false`.
- dev: prettier output for `just test-tokens`

## v0.7.0 - 08.01.2024 - Many cli improvements

- common options like `-v`, `--force`, `-O`, etc. are now available in subcommands as well. This means, one can now e.g. specify the output format at the end `atl ls -a -O timeclock` instead of the front `atl -O timeclock ls -a`.
    - **Note**: These options seem to have to appear **at the end** of the command-line. Trailing arguments won't get parsed correctly. `atl` shows a warning in this case.
- `atl --version[-only]` now shows version information
- `atl ls --month` fixed (it didn't use the first of the month as start)
- `atl tr`:
    - show the generated event before the (slow) committing
    - honor `-n|--dry-run` option to try out the parsing without storage
    - prevent modifying the `id` field (might be used later)
    - new time formats:
        ```
        the last 10min
        prev 2h
        next 2days
        last day
        last hour
        tomorrow
        today
        yesterday
        ```
- `atl del`: honor `--dry-run`
- `atl sync`: pass arguments to `git annex assist`, e.g. `atl sy -m "bulk changes"` sets custom commit message after many changes with `-c annextimelog.commit=false` set.
- development improvements:
    - added a [`justfile`](https://github.com/casey/just) for common dev tasks
    - added GitLab issue templates for bugs and feature requests

## v0.6.1 - 08.01.2023 - hotfix

- fix `TypeError: expected string or bytes-like object, got 'NoneType'` on `atl ls -a`

## v0.6.0 - 08.01.2023 - more time range formats, ignored filler words, no-emoji mode and other improvements here and there

- `atl tr`:
     - new time range formats e.g. `2h until now` or just a duration `30min` (implicit `until now`) and `since 30min` or `for 8h`
     - common prepositions/filler words (such as: with, near, on, etc.) are now ignored. This makes for more natural language in tracking, e.g. `atl tr work @home for 2h on project=A with person=pete and mood=yay`
- `atl ls --id-only` lists just the matched IDs (e.g. for piping into `xargs atl rm`)
- `atl rm`: better handling and warnings when matching multiple events to delete
- `atl git config annextimelog.emojis=false` or `atl -c emojis=false ...` will hide emojis in the pretty-formatted output. This may help if the terminal and the emoji font don't work well togetherand cause weird formatting glitches or when one just doesn't like emojis.
- more informative warnings and infos here and there (e.g. when `atl ls` didn't find any events, hints to debug with `-vvvv` in case of problems, etc.)
- increased test coverage and worked around Python 3.10's limited `fromisoformat()` function, fixing full date parsing
- added quick examples to `atl -h` help page

## v0.5.0 - 06.01.2023 - more natural time range parsing due to token system

- `atl tr`: improved time range syntax, e.g.
    ```bash
    atl tr y22:00 until now
    atl tr 2h10min ago - now
    atl tr 12h ago til 5min ago
    ...
    ```
- just `atl` (and `atl ls` and `atl su`) now correctly default to just the current day.
- `atl su -a` now lists all known events
- mypy happiness now asserted in CI
- `atl del`: allow deletion of multiple events
- annextimelog repo environment variable is now both `ANNEXTIMELOG_REPO` and `ANNEXTIMELOGREPO` (help text had an error)
- some other fixes and improvements here and there

## v0.4.0 - 03.01.2023 - cli output format and other small changes

- `now` is now accepted as a time, e.g. `atl tr 10:00 now work`
- `atl ls -l` now shows internal event git annex key and paths
- `atl -O cli ls` now outputs `atl tr ...` commands to remake the matched events
- In interactive Python session, `Event` objects now have a pretty representation as in the cli
- `poetry` is now used in CI
- Python versions 3.10 to 3.13 are now tested in CI
- At least Python 3.10 is now required

## v0.3.1 - 22.12.2023 - Update PyPI urls

- PyPI url updates

## v0.3.0 - 22.12.2023 - Getting closer to _'just about usable'_

- hooking into git to provide functionality
- `atm su` can now list events in a specified time frame
- events now show a pretty-formatted duration
- `atm del` to remove an event
- `atm test` to run (ridiculously empty) test suite
- several fixes here and there

## v0.2.1 - 21.12.2023 - `ImportError` hotfix

- Forgot a `import json` 🙄

## v0.2.0 - 21.12.2023 - Basic functionality

- basic infrastructure and cli around a git annex repo
- basic time tracking with `atl track`

## v0.1.0 - 20.12.2023 - Outline

- Initial version with only the project outline available, no real code yet
