# 🛠️ `annextimelog` Implementation

> These are notes on the initial design before the implementation. Most of it is implemented as documented here.

Surprisingly, many of the above requirements can be fulfilled without reinventing the wheel by employing [git-annex](https://git-annex.branchable.com), an extension to git that enables (among many other powerful file syncing things) *attaching metadata to files*.
Git Annex provides sophisticated mechanisms to sync (multiple) git repositories without interaction and can also resolve certain merge conflicts on its own.
I had the following design in mind, of which most is already implemented:

- Every **tracked period** (working on a project, sleeping, whatever) is represented by an annexed file.
    - This file is created with some random, *unique content and name* without further meaning (e.g. just a UUID).
    - The file's content will never change. In fact, it doesn't even matter and the file might be `git annex drop --force`d right after creation.
    - The file's name also stays the same. Renames could cause git merge conflicts.
    - Period files would have a common extension (e.g. `.t` to keep it short) so that git-annex can be instructed to not want to copy them around and complain if they are missing. (e.g. `git annex wanted . 'not include=*.t'`)
    - Period files would be (automatically/regularly) sorted into a `YYYY/MM/DD` as well as `YYYY/WW` (calendar week) folder structure and may appear in multiple locations if they overlap (e.g. when tracking sleep, a period likely spans two days). This allows for optimization of some queries and operations (e.g. showing only the current day, week or month). Git Annex still knows they're the same thing even if they're scattered all over the place.
    - Period metadata is stored as [git annex metadata](https://git-annex.branchable.com/design/metadata/):
        - Metadata can be merged without conflicts by git annex. The most recent change wins, but the history stays available.
        - Fields:
            - start and end time (ISO format UTC)
            - tags (`tag` metadata field)
            - arbitray metadata with values (cli with `atl track 08:00 work @home` would start tracking a work period starting at 08:00 local time and set the `location` metadata field to `home`)
                - one of those fields is the `note` field, which can be edited in the `$EDITOR` to add arbitrary tex, even with newlines, emojis, whatever, possibly encrypted (but not with git-annex's mechanism)
- The **cli** would be a bit similar to timewarrior's, and could look like this:
    - ```bash
      atl track 08:00 work @home            # start working from home (tags=work, location=home)
      atl track 08:00 work location=home    # same
      atl tag project1              # add a tag to currently running period
      atl note "bla"                # add a note to currently running period
      atl note                      # opens $EDITOR to edit note
      atl note -e "bla"             # same, but encrypt store note encrypted
      atl stop                      # stop current period tracking

      atl day|week|month            # show list/chart of tracked periods, with dynamic number (newest=1, etc.)
      atl note :3                   # edit note of the third-last tracked period

      atl track yesterday18:00 - 21:00 "coding session"   # track 3h of past event yesterday
      atl track y18:00 - 21:00 "coding session"           # shorter form
      atl track 2023-12-01T18:00 - 2023-12-01T21:00 "coding session" # timewarrior-style form

      atl sync                      # run 'git annex assist' to sync up
      ```
