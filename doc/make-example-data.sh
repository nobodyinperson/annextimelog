#!/usr/bin/env sh
atl config commit false
atl config fast true
atl config confirm false

# chatGPT-generated
atl track yyy10:00 yyy12:15 work @home coding project+=atl :"Working on CLI" ="Code Refactoring"
atl track yy10:30 yy11:15 personal where=outside exercise health+=running :"Ran 5k in the park" ="Morning Run"
atl track yyy12:00 yyy13:30 study reading @work subject+=machine-learning :"Read chapter 3 of ML textbook" ="Study Session"
atl track yyyyy09:30 yyyyy10:45 work meeting @online project+=client-a :"Client A Weekly Meeting" ="Project Updates"
atl track yy11:00 yy11:10 personal meditation @home health+=mindfulness :"10 minutes of mindfulness meditation" ="Morning Meditation"
atl track yyy10:15 yyy11:45 work coding @library project+=atl :"Implementing new feature" ="Feature Development"
atl track yyy9:45 yyy11:00 personal hobby @home @basement activity+=painting :"Started a new canvas painting" ="Artistic Expression"
atl track yyy11:30 yyy13:00 work research in=train project+=research :"Conducting market research" ="Market Analysis"
atl track yyyyy10:00 yyyyy11:30 personal in=town social event+=coffee-with-friends :"Coffee with old friends" ="Socializing"
atl track yyyyyyyyyyyy10:00 yyyyyyyyyyyy11:30 ="An old event"
atl track yy10:45 yy11:30 work coding project+=atl :"Bug fixing and testing" ="Quality Assurance"
atl track y10:00 y11:30 personal @home task project+=atl :"Testing ATL CLI" ="CLI Testing"
atl track 10:30 11:45 work coding @home project+=atl :"Debugging an issue" ="Debug Session"
atl track 13:00 14:30 personal hobby @home @garden activity+=gardening :"Planting flowers in the backyard" ="Gardening Time"
atl track last month ="notes of the month" :"cool month!"
atl track last year ="notes of the year" :"what a year!"

atl config --unset commit
atl config --unset fast
atl config --unset confirm
atl sync
